<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','HomeController@index');

Route::get('/register','AuthController@register');

Route::get('/welcome','AuthController@form');

Route::get('/', function(){
	return view('table.table');
});

Route:: get('/data-table', function(){
	return view('table.data-table');
});

/* Route untuk Tugas CRUD */
Route::get('/', function(){
	return view('crud.data-pertanyaan');
});

Route::get('/pertanyaan', function(){
	return view('crud.index');
});

Route::get('/pertanyaan/create','PertanyaanController@create');
Route::post('/pertanyaan/create','PertanyaanController@store');
Route::get('/pertanyaan','PertanyaanController@index');
Route::get('/pertanyaan/{pertanyaan_id}','PertanyaanController@show');
Route::get('/pertanyaan/{pertanyaan_id}/edit','PertanyaanController@edit');
Route::put('/pertanyaan/{pertanyaan_id}','PertanyaanController@update');
Route::delete('/pertanyaan/{pertanyaan_id}','PertanyaanController@destroy');
