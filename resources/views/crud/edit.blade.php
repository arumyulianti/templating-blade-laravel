@extends ('crud.layoutmaster')


@section ('title')
  Data Pertanyaan
@endsection

@section ('content')
  <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan {{ $post -> id }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{ $post -> id }}" method="POST">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{old('judul', $post->judul)}}" placeholder="Enter email">
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $post->isi)}}" placeholder="Isi">
                  </div>
                  <div class="form-group">
                    <label for="tanggal_dibuat">Tanggal Dibuat</label>
                    <input type="text" class="form-control" id="tanggal_dibuat" value="{{old('tanggal_dibuat', $post->tanggal_dibuat)}}" name="tanggal_dibuat" placeholder="Tanggal Dibuat">
                  </div>
                  <div class="form-group">
                    <label for="tanggal_diperbaharui">Tanggal Diperbaharui</label>
                    <input type="text" class="form-control" id="tanggal_diperbaharui" value="{{old('tanggal_diperbaharui', $post->tanggal_diperbaharui)}}" name="tanggal_diperbaharui" placeholder="Tanggal Diperbaharui">
                  </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
  </div>

  @endsection