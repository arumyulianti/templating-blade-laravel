@extends ('crud.layoutmaster')

@section ('title')
  Tabel Pertanyaan
@endsection

@section ('content')
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">#</th>
      <th>Judul</th>
      <th>Isi</th>
      <th>Tanggal Dibuat</th>
      <th>Tanggal Diperbaharui</th>
      <th style="width: 40px">Action</th>
    </tr>
  </thead>
  <tbody>
  	@foreach($posts as $key => $pertanyaan)
  	<tr>
  		<td> {{ $key + 1}} </td>
  		<td> {{ $pertanyaan -> judul }} </td>
  		<td> {{ $pertanyaan -> isi }} </td>
  		<td> {{ $pertanyaan -> tanggal_dibuat }} </td>
  		<td> {{ $pertanyaan -> tanggal_diperbaharui }} </td>
  		<td style="display: flex;"> 
  			<a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm"> Show </a>
  			<a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-info btn-sm"> Edit </a>  
        <form action="/pertanyaan/{{$pertanyaan->id}}" method="post">
        @csrf
        @method('DELETE')
          <input type="submit" value="Delete" class="btn btn-danger btn-sm">
  		</td> 

  	</tr>
  	@endforeach

</table>

@endsection