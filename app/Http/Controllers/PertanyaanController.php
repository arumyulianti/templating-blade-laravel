<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    //
    public function create(){
    	return view('crud.data-pertanyaan');
    }

    public function store(Request $request){
    	//dd($request->all());
    	$query= DB::table('pertanyaan')->insert([ //menambahkan data ke database
    			"judul" => $request["judul"],
    			"isi" => $request["isi"],
    			"tanggal_dibuat" => $request["tanggal_dibuat"],
    			"tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
		]);

		return redirect("/pertanyaan/create");
    }

    public function index(){
    	$posts = DB::table('pertanyaan')->get(); //mengambil data dari database
    	//dd($posts);
    	return view('crud.index', compact('posts')); 
    }

    public function show($id){
    	$post = DB::table('pertanyaan')->where('id', $id)->first(); //menampilkan detail pertanyaan dengan id tertentu
    	//dd($post);
    	return view('crud.show', compact('post')); 
    }

    public function edit($id){
    	$post = DB::table('pertanyaan')->where('id', $id)->first(); //edit 
    	//dd($post);
    	return view('crud.edit', compact('post')); 
    }

    public function update($id, request $request){ //update database 
    	$query = DB::table('pertanyaan')
    						->where('id', $id)
    						->update([
    							"judul" => $request["judul"],
				    			"isi" => $request["isi"],
				    			"tanggal_dibuat" => $request["tanggal_dibuat"],
				    			"tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
    						]); 
    	//dd($query);
    	return redirect('/pertanyaan')->with ('success','Berhasil Update Post'); 
    }

     public function destroy($id){ #delete
    	$query = DB::table('pertanyaan')->where('id', $id)->delete(); 
    	//dd($query);
    	return redirect('/pertanyaan')->with ('success','Berhasil Update Post');  
    }

}
